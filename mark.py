from tkinter import Canvas


def make_mark(x, y, r=5, vx=0, vy=0):
    return {
        "x": x,
        "y": y,
        "r": r,
        "vx": vx,
        "vy": vy,
        "filled": 0
    }


def mark_update(mark, dt):
    mark["x"] += mark["vx"] * dt
    mark["y"] += mark["vy"] * dt


def mark_draw(mark, canvas: Canvas):
    x = mark["x"]
    y = mark["y"]
    r = mark["r"]
    # HACK
    colour = '#%02x%02x%02x' % (255, 128, 0)
    # print(colour)
    #canvas.create_rectangle(x-r, y-r, x+r, y+r)
    if mark["filled"]:
        canvas.create_polygon(x-r, y-r, x+r, y+r, x-r, y+r)
    else:
        canvas.create_line(x-r, y-r, x+r, y+r, x-r, y +
                           r, x-r, y-r, fill=colour)
    if x <= 0 or x >= canvas.width:
        mark["vx"] = -mark["vx"]
    if y <= 0 or y >= canvas.height:
        mark["vy"] = -mark["vy"]
    # canvas.width

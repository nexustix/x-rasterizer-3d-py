
def make_mesh():
    return {
        "triangles": []
    }


def mesh_add_vec3d(mesh, v1, v2, v3):
    mesh["triangles"].append((v1+(1,), v2+(1,), v3+(1,)))


# def mesh_translate(mesh, dx=0, dy=0, dz=0):
#    for it, t in enumerate(mesh["triangles"]):
#        for ip, p in enumerate(t):
#            mesh["triangles"][it][ip] = (
#                mesh["triangles"][it][ip][0] + dx,
#                mesh["triangles"][it][ip][1] + dy,
#                mesh["triangles"][it][ip][2] + dz,
#            )


def mesh_generate_testcube():
    mesh = make_mesh()
    #mesh_add_vec3d(mesh, (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0))
    mesh_add_vec3d(mesh, (0.0, 0.0, 0.0), (0.0, 1.0, 0.0), (1.0, 1.0, 0.0))
    mesh_add_vec3d(mesh, (0.0, 0.0, 0.0), (1.0, 1.0, 0.0), (1.0, 0.0, 0.0))
    mesh_add_vec3d(mesh, (1.0, 0.0, 0.0), (1.0, 1.0, 0.0), (1.0, 1.0, 1.0))
    mesh_add_vec3d(mesh, (1.0, 0.0, 0.0), (1.0, 1.0, 1.0), (1.0, 0.0, 1.0))
    mesh_add_vec3d(mesh, (1.0, 0.0, 1.0), (1.0, 1.0, 1.0), (0.0, 1.0, 1.0))
    mesh_add_vec3d(mesh, (1.0, 0.0, 1.0), (0.0, 1.0, 1.0), (0.0, 0.0, 1.0))
    mesh_add_vec3d(mesh, (0.0, 0.0, 1.0), (0.0, 1.0, 1.0), (0.0, 1.0, 0.0))
    mesh_add_vec3d(mesh, (0.0, 0.0, 1.0), (0.0, 1.0, 0.0), (0.0, 0.0, 0.0))
    mesh_add_vec3d(mesh, (0.0, 1.0, 0.0), (0.0, 1.0, 1.0), (1.0, 1.0, 1.0))
    mesh_add_vec3d(mesh, (0.0, 1.0, 0.0), (1.0, 1.0, 1.0), (1.0, 1.0, 0.0))
    mesh_add_vec3d(mesh, (1.0, 0.0, 1.0), (0.0, 0.0, 1.0), (0.0, 0.0, 0.0))
    mesh_add_vec3d(mesh, (1.0, 0.0, 1.0), (0.0, 0.0, 0.0), (1.0, 0.0, 0.0))
    return mesh


def mesh_load_obj(filepath):
    mesh = make_mesh()

    vs = []
    vs.append((0, 0, 0))

    with open(filepath, "r") as h:
        lines = h.readlines()
        for line in lines:
            segs = line.split(" ")
            if segs[0] == "v":
                vs.append(
                    (
                        float(segs[1]),
                        float(segs[2]),
                        float(segs[3])
                    )
                )
            elif segs[0] == "f":
                mesh_add_vec3d(mesh,
                               vs[int(segs[1])],
                               vs[int(segs[2])],
                               vs[int(segs[3])]
                               )

    return mesh

import threed.matrix as mutil
import threed.vector as vutil


def translate_triangle(triangle, dx=0, dy=0, dz=0):
    return (
        (triangle[0][0]+dx, triangle[0][1]+dy, triangle[0][2]+dz, 1),
        (triangle[1][0]+dx, triangle[1][1]+dy, triangle[1][2]+dz, 1),
        (triangle[2][0]+dx, triangle[2][1]+dy, triangle[2][2]+dz, 1)
    )


def transform_triangle(triangle, matrix):
    return (
        mutil.matrix4x4_vector3_mul(matrix, triangle[0]),
        mutil.matrix4x4_vector3_mul(matrix, triangle[1]),
        mutil.matrix4x4_vector3_mul(matrix, triangle[2])
    )


def scale_triangle(triangle, screen_width, screen_height):
    # XXX lone parens on line are a horrendous waste of space
    return (
        (
            (triangle[0][0]+1) * 0.5 * screen_width,
            (triangle[0][1]+1) * 0.5 * screen_height,
            (triangle[0][2]), 1
        ),
        (
            (triangle[1][0]+1) * 0.5 * screen_width,
            (triangle[1][1]+1) * 0.5 * screen_height,
            (triangle[1][2]), 1
        ),
        (
            (triangle[2][0]+1) * 0.5 * screen_width,
            (triangle[2][1]+1) * 0.5 * screen_height,
            (triangle[2][2]), 1
        )
    )


def get_triangle_normal(triangle):
    l1 = vutil.vector3_vector3_sub(triangle[1], triangle[0])
    l2 = vutil.vector3_vector3_sub(triangle[2], triangle[0])

    normal = vutil.vector3_vector3_cross_product(l1, l2)
    normal_normal = vutil.vector3_normalize(normal)
    return normal_normal

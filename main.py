import thirdparty.graphics as gfx

from mark import *
from threed.mesh import *
# import threed.projection as prj
# import threed.rotation as rot
import threed.vector as vutil
import threed.matrix as mutil
import threed.colour as cutil
import threed.triangle as tutil
import threed.matrix_templates as mgen

import random
import math
from datetime import datetime, timedelta

sw = 800
sh = 600

# win = gfx.GraphWin("python \"graphics.py\" 3D test", 800, 600, autoflush=False)
win = gfx.GraphWin("3D test", sw, sh, autoflush=False)

time_last = datetime.now()

nblack = '#%02x%02x%02x' % (16, 16, 16)
norange = '#%02x%02x%02x' % (255, 128, 0)

# tmesh = make_mesh()
# tmesh = mesh_generate_testcube()
# gpmat = prj.make_projection_matrix(sw, sh)
gpmat = mgen.make_projection_matrix(sw, sh)

tmesh = mesh_load_obj("assets/Utah_teapot_smaller_er.obj")
# tmesh = mesh_load_obj("scrumbleship_small.obj")


# def prepare_triangle_old(triangle, theta, pmat, screen_width, screen_height, camera=[0, 0, 0, 1]):
#    # pmat = prj.make_projection_matrix(screen_width, screen_height)
#    # FIXME matrices don't need to be re-generated for each triangle
#    # rmatx = mgen.make_rotation_matrix_x(theta * 0.5)
#    # rmatz = mgen.make_rotation_matrix_z(theta)
#
#    # rotated = tutil.transform_triangle(triangle, rmatz)
#    # rotated2 = tutil.transform_triangle(rotated, rmatx)
#
#    rotated_fin = triangle
#
#    # translated = translate_triangle(rotated_fin, dz=3)
#    # translated = translate_triangle(rotated_fin, dz=8)
#    # translated = translate_triangle(rotated_fin, dz=16)
#    translated = tutil.translate_triangle(rotated_fin, dz=16)
#
#    # normal_normal = get_triangle_normal(translated)
#    normal_normal = tutil.get_triangle_normal(translated)
#
#    # visible = vutil.dot_product_v3(
#    visible = vutil.vector3_vector3_dot_product(
#        # normal_normal, vutil.difference_v3(translated[0], camera))
#        normal_normal, vutil.vector3_vector3_sub(translated[0], camera))
#
#    # projected = transform_triangle(translated, pmat)
#    projected = tutil.transform_triangle(translated, pmat)
#
#    # scale into view
#    nprojected = (
#        vutil.vector3_div(projected[0], projected[0][3]),
#        vutil.vector3_div(projected[1], projected[1][3]),
#        vutil.vector3_div(projected[2], projected[2][3]),
#        1
#    )
#
#    # scaled = scale_triangle(nprojected, screen_width, screen_height)
#    scaled = tutil.scale_triangle(nprojected, screen_width, screen_height)
#    return scaled, normal_normal, visible


def prepare_triangle(triangle, rmat, camera):
    rotated = tutil.transform_triangle(triangle, rmat)
    translated = tutil.translate_triangle(rotated, dz=16)
    normal = tutil.get_triangle_normal(translated)
    visible = vutil.vector3_vector3_dot_product(
        normal, vutil.vector3_vector3_sub(translated[0], camera))
    if visible < 0:
        return translated, normal
    return None


def project_triangle(triangle, pmat, screen_width, screen_height):
    projected = tutil.transform_triangle(triangle, pmat)
    # scale into view
    nprojected = (
        vutil.vector3_div(projected[0], projected[0][3]),
        vutil.vector3_div(projected[1], projected[1][3]),
        vutil.vector3_div(projected[2], projected[2][3]),
        1
    )
    scaled = tutil.scale_triangle(nprojected, screen_width, screen_height)
    return scaled


def draw_triangle(canvas, triangle, fill=False, colour="white"):
    if fill:
        canvas.create_polygon(dnt[0][0], dnt[0][1], dnt[1][0],
                              dnt[1][1], dnt[2][0], dnt[2][1], dnt[0][0], dnt[0][1], fill=colour)
    else:
        win.create_line(dnt[0][0], dnt[0][1], dnt[1][0],
                        dnt[1][1], dnt[2][0], dnt[2][1], dnt[0][0], dnt[0][1], fill=colour)


def get_triangle_depth(triangle):
    return (triangle[0][2] + triangle[1][2] + triangle[2][2]) / 3
    # return int((triangle[0][2] + triangle[1][2] + triangle[2][2]) / 3)


camera = [0, 0, 0, 1]
# light_dir = vutil.normalize_v3([0, 0, -1])
light_dir = vutil.vector3_normalize((-1, -1, -1))
# print(light_dir)
theta = 0
while not win.closed:
    time_now = datetime.now()
    time_delta = time_now - time_last
    dt = time_delta.total_seconds()
    fps = math.floor(1/dt)
    time_last = time_now

    theta += 1 * dt

    rmatx = mgen.make_rotation_matrix_x(theta * 0.5)
    rmatz = mgen.make_rotation_matrix_z(theta)
    rmato = mutil.matrix4x4_matrix4x4_mul(rmatx, rmatz)

    # HACK
    win.delete("all")
    win.create_rectangle(0, 0, win.width, win.height, fill=nblack)

    render_triangles = []

    for t in tmesh["triangles"]:
        # dnt, normal, visible = prepare_triangle(t, theta, gpmat, sw, sh)
        # dnt = list(dnt) + [normal]
        # if visible < 0:
        #    render_triangles.append(dnt)
        r = prepare_triangle(t, rmato, camera)
        if r:
            # dnt = list(r[0]) + r[1]
            dnt = project_triangle(r[0], gpmat, sw, sh) + (r[1],)
            render_triangles.append(dnt)

    sorted_triangles = sorted(
        render_triangles, key=get_triangle_depth, reverse=True)

    for i, dnt in enumerate(sorted_triangles):
        # for dnt in render_triangles:
            # normal = render_normals[i]
        normal = dnt[3]

        # print(light)
        # win.create_polygon(dnt[0][0], dnt[0][1], dnt[1][0],
        #                   dnt[1][1], dnt[2][0], dnt[2][1], dnt[0][0], dnt[0][1], fill="yellow")
        # if normal[2] <= 0:
        # if visible < 0:
        # light = (vutil.dot_product_v3(light_dir, normal) + 1)/2
        light = (vutil.vector3_vector3_dot_product(light_dir, normal) + 1)/2
        draw_triangle(win, dnt, fill=True,
                      colour=cutil.gen_colour_shade((255, 128, 0), light))
        # draw_triangle(win, dnt, fill=True, colour=norange)
        # draw_triangle(win, dnt, fill=False, colour=norange)
        # draw_triangle(win, dnt, fill=False, colour=nblack)
        # win.create_line(dnt[0][0], dnt[0][1], dnt[1][0],
        #                dnt[1][1], dnt[2][0], dnt[2][1], dnt[0][0], dnt[0][1], fill=norange)

    # win.create_rectangle(10, 10, 64, 40, fill="black")
    win.create_text(40, 20, text="FPS:"+str(fps), fill=norange)
    win.update()

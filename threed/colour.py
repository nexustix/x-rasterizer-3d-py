import math


def lerp(start, stop, t):
    return start * (1 - t) + stop * t


def gen_colour_shade(colour, t):

    colour = colour or (255, 255, 255)

    r = math.floor(lerp(0, colour[0], t))
    g = math.floor(lerp(0, colour[1], t))
    b = math.floor(lerp(0, colour[2], t))

    return '#%02x%02x%02x' % (r, g, b)

import math


def make_projection_matrix(screen_width, screen_height):
    znear = 0.1
    zfar = 1000.0
    fov = 90.0
    a = screen_height/screen_width  # TODO reverse ?
    # a = screen_width/screen_height  # TODO reverse ?
    f = 1.0 / math.tan(fov * 0.5 / 180.0 * 3.14159)
    q = zfar / (zfar-znear)
    oq = (-zfar * znear) / (zfar - znear)

    #mat = [[0]*4]*4

    mat = (
        (a*f, 0, 0, 0),
        (0, f, 0, 0),
        (0, 0, q, 1),
        (0, 0, oq, 0)
    )

    return mat


def make_rotation_matrix_x(theta):
    co = math.cos(theta)
    si = math.sin(theta)

    mat = (
        (1, 0, 0, 0),
        (0, co, -si, 0),
        (0, si, co, 0),
        (0, 0, 0, 1),
    )

    return mat


def make_rotation_matrix_y(theta):
    co = math.cos(theta)
    si = math.sin(theta)

    mat = (
        (co, 0, si, 0),
        (0, 1, 0, 0),
        (-si, 0, co, 0),
        (0, 0, 0, 1),
    )

    return mat


def make_rotation_matrix_z(theta):
    co = math.cos(theta)
    si = math.sin(theta)

    mat = (
        (co, -si, 0, 0),
        (si, co, 0, 0),
        (0, 0, 1, 0),
        (0, 0, 0, 1),
    )

    return mat


def make_matrix_identity():
    return (
        (1, 0, 0, 0),
        (0, 1, 0, 0),
        (0, 0, 1, 0),
        (0, 0, 0, 1)
    )

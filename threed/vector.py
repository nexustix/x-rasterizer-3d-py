import math
import threed.decoration as dec

"""
# optional numba test

numba = None
try:
    # import numba # optional to make it run fasta
    pass
except:
    pass


def jit(**args):
    return lambda **largs: None


if numba:
    jit = numba.jit
"""

# @numba.jit(nopython=True, fastmath=True)


# @dec.maybe(jit(nopython=True, fastmath=True), numba)
def vector3_magnitude(a):
    # HACK causes unneeded temporary variables
    return math.sqrt(vector3_vector3_dot_product(a, a))


# @dec.maybe(jit(nopython=True, fastmath=True), numba)
def vector3_normalize(a):
    t = math.sqrt(a[0]**2 + a[1]**2 + a[2]**2) or 1
    return (
        a[0]/t,
        a[1]/t,
        a[2]/t,
        1  # HACK
    )

# def vector3_add():
#    pass
#
#
# def vector3_sub():
#    pass


# @dec.maybe(jit(nopython=True, fastmath=True), numba)
def vector3_mul(a, n):
    return (
        a[0] * n,
        a[1] * n,
        a[2] * n
    )


# @dec.maybe(jit(nopython=True, fastmath=True), numba)
def vector3_div(a, n):
    return (
        a[0] / n,
        a[1] / n,
        a[2] / n
    )


# @dec.maybe(jit(nopython=True, fastmath=True), numba)
def vector3_vector3_add(a, b):
    return (
        a[0] + b[0],
        a[1] + b[1],
        a[2] + b[2],
        1  # HACK
    )


# @dec.maybe(jit(nopython=True, fastmath=True), numba)
def vector3_vector3_sub(a, b):
    return (
        a[0] - b[0],
        a[1] - b[1],
        a[2] - b[2],
        1  # HACK
    )


# @dec.maybe(jit(nopython=True, fastmath=True), numba)
def vector3_vector3_dot_product(a, b):
    return (
        a[0] * b[0] +
        a[1] * b[1] +
        a[2] * b[2]
    )


# @dec.maybe(jit(nopython=True, fastmath=True), numba)
def vector3_vector3_cross_product(a, b):
    return (
        a[1] * b[2] - a[2] * b[1],
        a[2] * b[0] - a[0] * b[2],
        a[0] * b[1] - a[1] * b[0],
        1  # HACK
    )

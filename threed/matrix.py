# def matrix4x4_vector3_mul_old(m, iv):
#    return (
#        (iv[0] * m[0][0]) + (iv[1] * m[1][0]) +
#        (iv[2] * m[2][0]) + (iv[3] * m[3][0]),
#        (iv[0] * m[0][1]) + (iv[1] * m[1][1]) +
#        (iv[2] * m[2][1]) + (iv[3] * m[3][1]),
#        (iv[0] * m[0][2]) + (iv[1] * m[1][2]) +
#        (iv[2] * m[2][2]) + (iv[3] * m[3][2]),
#        (iv[0] * m[0][3]) + (iv[1] * m[1][3]) +
#        (iv[2] * m[2][3]) + (iv[3] * m[3][3]),
#    )


def matrix4x4_vector3_mul(m, iv):
    # TODO returns vector4, not sure if cleaner
    return (
        (iv[0] * m[0][0]) + (iv[1] * m[1][0]) +
        (iv[2] * m[2][0]) + (1 * m[3][0]),
        (iv[0] * m[0][1]) + (iv[1] * m[1][1]) +
        (iv[2] * m[2][1]) + (1 * m[3][1]),
        (iv[0] * m[0][2]) + (iv[1] * m[1][2]) +
        (iv[2] * m[2][2]) + (1 * m[3][2]),
        (iv[0] * m[0][3]) + (iv[1] * m[1][3]) +
        (iv[2] * m[2][3]) + (1 * m[3][3])
    )


def matrix4x4_matrix4x4_mul(a, b):
    mat = ()
    tmat = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    for c in range(4):
        for r in range(4):
            val = (a[r][0] * b[0][c]) + (a[r][1] * b[1][c]) + \
                (a[r][2] * b[2][c]) + (a[r][3] * b[3][c])
            tmat[r][c] = val
            #print(r, c, val)
    for r in tmat:
        mat = mat + (tuple(r),)

    return mat

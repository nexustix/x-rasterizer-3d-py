

def maybe(dec, cond):
    """ conditional decorator
    """
    def result_dec(f):
        if not cond:
            return f
        return dec(f)
    return result_dec
